INSTALL

1. clone repository or download archive
2. find file .env and copy .env.local
3. find page `DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7`
and set your mysql params
4. run php composer.phar install
5. run php bin/console doctrine:database:create
6. run php bin/console doctrine:migrations:migrate
7. install symfony web server `https://symfony.com/doc/4.4/setup/symfony_server.html`
or run your local web server (apache/nginx) 