<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductDetailFieldRepository;
use App\Repository\ProductDetailValueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductDetailFieldFormService
{
    private $detailValueRepository;
    private $detailFieldRepository;
    private $formFactory;

    public function __construct(
        ProductDetailFieldRepository $detailFieldRepository,
        ProductDetailValueRepository $detailValueRepository,
        FormFactoryInterface $formFactory
    ) {
        $this->detailValueRepository = $detailValueRepository;
        $this->detailFieldRepository = $detailFieldRepository;
        $this->formFactory = $formFactory;
    }

    public function get(?Product $product): ?Form
    {
        $items = $this->detailFieldRepository->findAll();
        $form = $this->formFactory->create(FormType::class);
        foreach ($items as $index => $item) {
            $data = $this->detailValueRepository->getEntity($item, $product)->getValue();

            $form->add($item->getId(), TextType::class, [
                'label' => $item->getTitle(),
                'data' => $data,
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ],

            ]);
        }

        return $form;
    }

    public function save(array $formData, Product $product, EntityManagerInterface $entityManager): void
    {
        if (empty($formData)) {
            return;
        }

        foreach ($formData as $formFieldId => $value) {
            $fieldEntity = $this->detailFieldRepository->find($formFieldId);
            $entity = $this->detailValueRepository->getEntity($fieldEntity, $product, $value);
            $entityManager->persist($entity);
        }
    }
}
