import '../css/global.scss';
import $ from 'jquery';

$(function () {
    $('#success-message').hide(4000);
    $('#error-message').hide(9000);
});