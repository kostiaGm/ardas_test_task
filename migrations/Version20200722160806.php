<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200722160806 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Base data';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("SET foreign_key_checks = 0");
        $this->addSql("INSERT INTO product_detail_field (id, title) VALUES (NULL, 'Тип')");
        $this->addSql("INSERT INTO product_detail_field (id, title) VALUES (NULL, 'Рекомендуемая площадь помещения');");
        $this->addSql("INSERT INTO product_detail_field (id, title) VALUES (NULL, 'Тип компрессора')");

        $this->addSql("INSERT INTO product (id, name, price, created_at) VALUES (NULL, 'Samurai SMA-07HRDN1 ION', 6700, '2020-07-22 15:17:22')");
        $this->addSql("INSERT INTO product (id, name, price, created_at) VALUES (NULL, 'Cooper&Hunter CH-S09FTXLA-NG Arctic Wi-Fi', 17000, '2020-07-22 15:18:25')");
        $this->addSql("INSERT INTO product (id, name, price, created_at) VALUES (NULL, 'IdeaPro IPA-12HRFN1 ION Brilliant DC', 12050, '2020-07-22 15:19:07')");


        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 1, 3, 'Сплит-система')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 2, 3, '35')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 3, 3, 'Инверторный')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 1, 2, 'Сплит-система')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 2, 2, '25')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 3, 2, 'Не указано')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 1, 1, 'Переносной')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 2, 1, '15')");
        $this->addSql("INSERT INTO product_detail_value (id, field_id, product_id, value) VALUES (NULL, 3, 1, 'Инверторный')");

        $this->addSql("SET foreign_key_checks = 1");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("SET foreign_key_checks = 0");
        $this->addSql("TRUNCATE TABLE product");
        $this->addSql("TRUNCATE TABLE product_detail_field");
        $this->addSql("SET foreign_key_checks = 1");
    }
}
