<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ProductRepository;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Assert\PositiveOrZero
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=ProductDetailValue::class, mappedBy="product",  cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $productDetailValues;


    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->productDetailValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function removeField(ProductDetailField $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            // set the owning side to null (unless already changed)
            if ($field->getProduct() === $this) {
                $field->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductDetailValue[]
     */
    public function getProductDetailValues(): Collection
    {
        return $this->productDetailValues;
    }

    public function addProductDetailValue(ProductDetailValue $productDetailValue): self
    {
        if (!$this->productDetailValues->contains($productDetailValue)) {
            $this->productDetailValues[] = $productDetailValue;
            $productDetailValue->setProduct($this);
        }

        return $this;
    }

    public function removeProductDetailValue(ProductDetailValue $productDetailValue): self
    {
        if ($this->productDetailValues->contains($productDetailValue)) {
            $this->productDetailValues->removeElement($productDetailValue);
            // set the owning side to null (unless already changed)
            if ($productDetailValue->getProduct() === $this) {
                $productDetailValue->setProduct(null);
            }
        }

        return $this;
    }
}
