<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductFindType;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Service\ProductDetailFieldFormService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    private $detailFieldFormService;

    public function __construct(ProductDetailFieldFormService $detailFieldFormService)
    {
        $this->detailFieldFormService = $detailFieldFormService;
    }

    /**
     * @Route("/", name="product_index", methods={"GET","POST"})
     */
    public function index(
        Request $request,
        ProductRepository $productRepository,
        PaginatorInterface $paginator
    ): Response {

        $searchForm = $this->createForm(ProductFindType::class);
        $searchForm->handleRequest($request);

        $searchWord = null;
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchWord = $searchForm->getData()['q'];
        }

        $pagination = $paginator->paginate(
            $productRepository->getAllQuery($searchWord),
            $request->query->getInt('page', 1),
            10
        );


        return $this->render('product/index.html.twig', [
            'products' => $pagination,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/new", name="product_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        return $this->save($request, new Product(), 'product/new.html.twig');
    }

    /**
     * @Route("/{id}", name="product_show", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Product $product): Response
    {
        return $this->save($request, $product, 'product/edit.html.twig');
    }

    /**
     * @Route("/{id}/delete", name="product_delete", methods={"GET"})
     */
    public function delete(Request $request, Product $product): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        $this->addFlash('success', 'Data deleted');
        return $this->redirectToRoute('product_index');
    }

    private function save(Request $request, Product $product, string $render): ?Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        $this->createFormBuilder();
        $productFieldsForm = $this->detailFieldFormService->get($product);
        $productFieldsForm->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $productFieldsForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $this->detailFieldFormService->save($productFieldsForm->getData(), $product, $entityManager);
            $entityManager->flush();

            $this->addFlash('success', 'Data saved');
            return $this->redirectToRoute('product_index');
        }

        return $this->render($render, [
            'product' => $product,
            'form' => $form->createView(),
            'productDetailForm' => $productFieldsForm->createView()
        ]);
    }

}

