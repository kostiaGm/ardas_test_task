<?php

namespace App\Entity;

use App\Repository\ProductDetailValueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductDetailValueRepository::class)
 */
class ProductDetailValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=ProductDetailField::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $field;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productDetailValues", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $product;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getField(): ?ProductDetailField
    {
        return $this->field;
    }

    public function setField(?ProductDetailField $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
