<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200720180201 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_detail_field (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(65) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_detail_value (id INT AUTO_INCREMENT NOT NULL, field_id INT NOT NULL, product_id INT DEFAULT NULL, value VARCHAR(150) NOT NULL, INDEX IDX_ED23B106443707B0 (field_id), INDEX IDX_ED23B1064584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_detail_value ADD CONSTRAINT FK_ED23B106443707B0 FOREIGN KEY (field_id) REFERENCES product_detail_field (id)');
        $this->addSql('ALTER TABLE product_detail_value ADD CONSTRAINT FK_ED23B1064584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_detail_value DROP FOREIGN KEY FK_ED23B1064584665A');
        $this->addSql('ALTER TABLE product_detail_value DROP FOREIGN KEY FK_ED23B106443707B0');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_detail_field');
        $this->addSql('DROP TABLE product_detail_value');
    }
}
