<?php

namespace App\Controller;

use App\Entity\ProductDetailField;
use App\Form\ProductDetailFieldType;
use App\Repository\ProductDetailFieldRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product/detail/field")
 */
class ProductDetailFieldController extends AbstractController
{
    /**
     * @Route("/", name="product_detail_field_index", methods={"GET"})
     */
    public function index(
        Request $request,
        ProductDetailFieldRepository $productDetailFieldRepository,
        PaginatorInterface $paginator
    ): Response {
        $pagination = $paginator->paginate(
            $productDetailFieldRepository->getAllQuery(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'product_detail_field/index.html.twig',
            [
                'product_detail_fields' => $pagination,
            ]
        );
    }

    /**
     * @Route("/new", name="product_detail_field_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $productDetailField = new ProductDetailField();
        return $this->save($request, $productDetailField, 'product_detail_field/new.html.twig');
    }

    /**
     * @Route("/{id}", name="product_detail_field_show", methods={"GET"})
     */
    public function show(ProductDetailField $productDetailField): Response
    {
        return $this->render(
            'product_detail_field/show.html.twig',
            [
                'product_detail_field' => $productDetailField,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="product_detail_field_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ProductDetailField $productDetailField): Response
    {
        return $this->save($request, $productDetailField, 'product_detail_field/edit.html.twig');
    }

    /**
     * @Route("/{id}/delete", name="product_detail_field_delete", methods={"GET"})
     */
    public function delete(Request $request, ProductDetailField $productDetailField): Response
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($productDetailField);
            $entityManager->flush();
            $this->addFlash('success', 'Data deleted');
        } catch (ForeignKeyConstraintViolationException $exception) {
            $this->addFlash('error',
                            'You cannot delete this field until you delete the product with this field');
        } catch (\Exception $exception) {
            $this->addFlash('error', 'Delete error!');
        }

        return $this->redirectToRoute('product_detail_field_index');
    }

    private function save(Request $request, ProductDetailField $productDetailField, string $render): Response
    {
        $form = $this->createForm(ProductDetailFieldType::class, $productDetailField);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($productDetailField);
            $entityManager->flush();
            $this->addFlash('success', 'Data saved');
            return $this->redirectToRoute('product_detail_field_index');
        }

        return $this->render(
            $render,
            [
                'product_detail_field' => $productDetailField,
                'form' => $form->createView(),
            ]
        );
    }
}
