<?php

namespace App\Repository;

use App\Entity\ProductDetailField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

class ProductDetailFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductDetailField::class);
    }

    public function getAllQuery(): Query
    {
        return $this
            ->createQueryBuilder('pf')
            ->orderBy('pf.id', 'DESC')
            ->getQuery();
    }
}
