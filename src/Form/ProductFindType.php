<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductFindType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('go', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-light float-right ml-3'
                ]
            ])
            ->add('q', TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'search product ...',
                'class' => 'form-control w-50 float-right'
            ]
        ]);
    }
}
