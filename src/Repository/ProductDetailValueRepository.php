<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductDetailField;
use App\Entity\ProductDetailValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductDetailValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductDetailValue::class);
    }

    public function getEntity(ProductDetailField $field, Product $product, ?string $value = null): ProductDetailValue
    {
        $ret = $this->findOneBy(["field"=>$field, "product" => $product]);
        if (empty($ret)) {
            $ret = new ProductDetailValue();
            $ret->setField($field);
            $ret->setProduct($product);
        }
        if ($value !== null) {
            $ret->setValue($value);
        }
        return $ret;
    }
}
