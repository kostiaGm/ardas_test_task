<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getAllQuery(?string $searchWord = null): Query
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC');

        if (!empty($searchWord)) {
            $queryBuilder
                ->where("p.name LIKE :word")
                ->setParameter('word', "%$searchWord%");
        }
        return $queryBuilder->getQuery();
    }
}
